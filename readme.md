﻿# Get-AzureADMFAStatus

Get-AzureADMFAStatus.ps1 gets MFA Info from Azure AD Tenant. It uses MSOnline Module. The following are the usage info:

#### *Get Enabled Member User MFA Status*

.\Get-AzureMFAStatus.ps1 -UserType EnabledOnly

#### *Get Disabled Member User MFA Status*

.\Get-AzureMFAStatus.ps1 -UserType DisabledOnly

#### *Get All Member User MFA Status*

.\Get-AzureMFAStatus.ps1 -UserType All

#### *Get All Member User MFA Status and output to CSV File stores in C:\Temp*

.\Get-AzureMFAStatus.ps1 -UserType All -CSVFile C:\Temp
