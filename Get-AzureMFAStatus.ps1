
<#PSScriptInfo

.VERSION 1.0.0

.GUID 156c52ca-1610-4d0e-92dd-a2d5b61212de

.AUTHOR Derek Luk

.COMPANYNAME EMS

.COPYRIGHT 

.TAGS 

.LICENSEURI 

.PROJECTURI 

.ICONURI 

.EXTERNALMODULEDEPENDENCIES 

.REQUIREDSCRIPTS 

.EXTERNALSCRIPTDEPENDENCIES 

.RELEASENOTES


#>

#

<# 

.DESCRIPTION 
 Get MFA Status from Azure AD 

 Usage:
 # Get Enabled Member User MFA Status
 .\Get-AzureMFAStatus.ps1 -UserType EnabledOnly

 # Get Disabled Member User MFA Status
 .\Get-AzureMFAStatus.ps1 -UserType DisabledOnly

 # Get All Member User MFA Status
 .\Get-AzureMFAStatus.ps1 -UserType All

 # Get All Member User MFA Status and output to CSV File stores in C:\Temp
 .\Get-AzureMFAStatus.ps1 -UserType All -CSVFile C:\Temp


#> 
Param(
    [Parameter(Mandatory=$true)]
    [Validateset('EnabledOnly','DisabledOnly','All')]
    $UserType,
    [Parameter(Mandatory=$false)]
    [ValidateScript({
        If(Test-Path $_){$true}else{Throw "Path: $_ does not exist"}}
    )]
    $CSVPath
)

#Variables
#region
$VerbosePre=$VerbosePreference
$VerbosePreference="Continue"
$EABackup=$ErrorActionPreference
$ErrorActionPreference="Stop"
$FinalObject=[System.Collections.ArrayList]@()
#endregion

#Check MSOnline Available and download from PS Gallery
#region
If (!(Get-Module -ListAvailable | where-Object {$_.Name -eq "MSOnline"}))
{
    Write-Verbose "Install MSOnline from PS Gallery"
    Install-Module -Name MSOnline -Scope CurrentUser -force
}
#endregion

#Check Connection and Connect to Azure AD
#region
If (!(Get-MsolDomain -ErrorAction SilentlyContinue))
{
    Write-Verbose "Connect to Azure AD"
    try {
    Connect-MsolService 
    } Catch {Write-Error "Failed to connect to Azure AD"}
}
#endregion

#Query Users (Member) and check MFA Status
#region
$DateFormat=Get-Date -Format MMddyyyy-HHmmss

#Query Users Info based on UserType Input
switch ($UserType)
{
    "EnabledOnly" {
        $Users=Get-MsolUser -EnabledFilter EnabledOnly -All 
        $CSVFile="EnabledUser_$DateFormat.csv"
    }
    "DisabledOnly" {
        $Users=Get-MsolUser -EnabledFilter DisabledOnly -All
        $CSVFile="DisabledUser_$DateFormat.csv"
    }
    "All" {
        $Users=Get-MsolUser -All
        $CSVFile="AllUser_$DateFormat.csv"
    }
}

#Filter Users to Member only
Write-Verbose "Queried $UserType Users"
$Users=$Users | where-Object {$_.UserType -eq "Member"}

#Loop through each User, Get MFA Info and output to Object
foreach ($User in $Users)
{
    #Reset Variables
    $Variables=@("MFAStatus")
    foreach ($Variable in $Variables)
    {
        If (Get-Variable $Variable -ErrorAction SilentlyContinue)
        {
            Clear-Variable $Variable
        }
    }
    #endregion

    #Check User MFA Status
    Write-Verbose "Checking $($User.DisplayName) MFA Status"
    If ($User.StrongAuthenticationMethods.IsDefault -eq $true)
    {
        $MFAStatus=($User.StrongAuthenticationMethods | where-object IsDefault -eq $true).Methodtype
    } else {$MFAStatus="Disabled"}
    
    #Output Info to PS Custom Object
    $UserInfo=[PSCustomobject]@{
        "DisplayName"=$User.Displayname
        "UserPrincipalName"=$User.UserPrincipalName
        "Title"=$User.Title
        "MFA_Status"=$MFAStatus
        "MFA_State"=$User.StrongAuthenticationRequirements.State
        "MFA_PhoneNumber"=$User.StrongAuthenticationUserDetails.PhoneNumber
        "MFA_AltPhoneNumber"=$User.StrongAuthenticationUserDetails.AlternativePhoneNumber
        "MFA_Email"=$User.StrongAuthenticationUserDetails.Email
        "IsLicensed"=$User.IsLicensed
    }
    $null=$FinalObject.Add($UserInfo)
}
#endregion

$VerbosePreference=$VerbosePre
$ErrorActionPreference=$EABackup

#Output Result to Screen
$FinalObject | sort-object -property UserPrincipalName

#Output Result to CSV if CSVPath exists
If ($CSVPath)
{
    $FinalObject | sort-object -property UserPrincipalName | export-csv "$CSVPath\$CSVFile" -NoTypeInformation
    Write-Output "CSV File Stored in $CSVPath\$CSVFile"
}
